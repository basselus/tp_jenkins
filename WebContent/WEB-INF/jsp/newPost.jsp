<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb"
	lang="en-gb">
	<head>
	
	<title>LOGIN</title>
	
	<link rel="stylesheet" href="fichiers/style.css" type="text/css" />
	</head>
	<body class="ltr">
	
		<div id="wrapcentre">
	
			<br style="clear: both;" />
	
				<%@ include file="header.jspf"%>
	
				<form action="#" method="post">
					<table class="tablebg" cellspacing="1" width="100%">
						<tbody>
							<tr>
								<th colspan="2">Login</th>
							</tr>

							<tr>
								<td class="row2">

									<table style="width: 100%;" cellspacing="1" cellpadding="4" align="center">
										<tbody>
											<tr>
												<td><input value="${param.id}" name="id" type="hidden" /></td>
												<td valign="top"><b class="gensmall">Message:</b></td>
												<td><textarea class="post" name="message" cols="200" tabindex="2" rows="20"></textarea></td>
											</tr>

										</tbody>
									</table>
								</td>
							</tr>

							<tr>
								<td class="cat" colspan="2" align="center"><input name="login" class="btnmain" value="Create !" tabindex="5" type="submit" /></td>
							</tr>
						</tbody>
					</table>

				</form>

				<table class="tablebg" style="margin-top: 5px;" cellspacing="1"
					cellpadding="0" width="100%">
					<tbody>
						<tr>
							<td class="row1">
								<p class="breadcrumbs">Index du forum</p>
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	</body>
</html>
