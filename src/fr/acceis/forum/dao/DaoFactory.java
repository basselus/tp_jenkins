package fr.acceis.forum.dao;

public class DaoFactory {
	
	public static IFilDeDiscussionDao getFilDeDiscussionDao() {
		return new FilDeDiscussionDao();
	}
	
	public static IPostDao getPostDao() {
		return new PostDao();
	}
	
	public static IUtilisateurDao getUtilisateurDao() {
		return new UtilisateurDao();
	}

}
