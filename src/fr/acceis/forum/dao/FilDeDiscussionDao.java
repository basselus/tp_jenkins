package fr.acceis.forum.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.acceis.forum.model.FilDeDiscussion;
import fr.acceis.forum.util_bis.HibernateUtil_bis;

public class FilDeDiscussionDao implements IFilDeDiscussionDao {

	public List<FilDeDiscussion> liste() {
		Session session = HibernateUtil_bis.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<FilDeDiscussion> criteria = criteriaBuilder.createQuery(FilDeDiscussion.class);
		Root<FilDeDiscussion> root = criteria.from(FilDeDiscussion.class);
		Query<FilDeDiscussion> query = session.createQuery(criteria);

		return query.getResultList();
	}

	public Long save(FilDeDiscussion fil) {
		Session session = HibernateUtil_bis.getInstance();
		Transaction tx = session.beginTransaction();
		try{
			Long result = (Long) HibernateUtil_bis.getInstance().save(fil);
			tx.commit();
			session.refresh(fil);
			return result;
		} catch (Exception e) {
			tx.rollback();
			throw e;
		}
	}

	public FilDeDiscussion getById(Long id) {
		return HibernateUtil_bis.getInstance().load(FilDeDiscussion.class, id);
	}

}
