package fr.acceis.forum.dao;

import java.util.List;

import fr.acceis.forum.model.FilDeDiscussion;

public interface IFilDeDiscussionDao {

	public List<FilDeDiscussion> liste();
	
	public Long save(FilDeDiscussion fil);

	public FilDeDiscussion getById(Long id);
	
}
