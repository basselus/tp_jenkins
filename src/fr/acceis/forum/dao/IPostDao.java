package fr.acceis.forum.dao;

import java.util.List;

import fr.acceis.forum.model.Post;

public interface IPostDao {
	
	public List<Post> liste();
	
	public Long save(Post post);

}
