package fr.acceis.forum.dao;

import fr.acceis.forum.model.Utilisateur;

public interface IUtilisateurDao {

	public Utilisateur getUtilisateurByLoginAndPassword(String login, String password);
	
}
