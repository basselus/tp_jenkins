package fr.acceis.forum.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.acceis.forum.model.Post;
import fr.acceis.forum.util_bis.HibernateUtil_bis;

public class PostDao implements IPostDao {

	public List<Post> liste() {
		Session session = HibernateUtil_bis.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Post> criteria = criteriaBuilder.createQuery(Post.class);
		Root<Post> root = criteria.from(Post.class);
		Query<Post> query = session.createQuery(criteria);
		
		return query.getResultList();
	}

	public Long save(Post post) {
		Session session = HibernateUtil_bis.getInstance();
		Transaction tx = session.beginTransaction();
		try{
			Long result = (Long) HibernateUtil_bis.getInstance().save(post);
			tx.commit();
			session.refresh(post);
			session.refresh(post.getFil());
			return result;
		} catch (Exception e) {
			tx.rollback();
			throw e;
		}
	}

}
