package fr.acceis.forum.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.acceis.forum.model.Utilisateur;
import fr.acceis.forum.util_bis.HibernateUtil_bis;

public class UtilisateurDao implements IUtilisateurDao {

	public Utilisateur getUtilisateurByLoginAndPassword(String login, String password) {
		
		Session session = HibernateUtil_bis.getInstance();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Utilisateur> criteria = criteriaBuilder.createQuery(Utilisateur.class);
		Root<Utilisateur> root = criteria.from(Utilisateur.class);
		criteria.where(criteriaBuilder.equal(root.get("login"), login), criteriaBuilder.equal(root.get("password"), password));
		Query<Utilisateur> query = session.createQuery(criteria);
		
		List<Utilisateur> resultList = query.getResultList();
		
		if (resultList.size() > 0) return resultList.get(0);
		return null;
		
	}
	
}
