package fr.acceis.forum.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Post")
public class Post {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Utilisateur auteur;
	
	private String texte;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private FilDeDiscussion fil;
	
	public FilDeDiscussion getFil() {
		return fil;
	}

	public void setFil(FilDeDiscussion fil) {
		this.fil = fil;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Utilisateur getAuteur() {
		return auteur;
	}

	public void setAuteur(Utilisateur auteur) {
		this.auteur = auteur;
	}

	public String getTexte() {
		return texte;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}
	
}
