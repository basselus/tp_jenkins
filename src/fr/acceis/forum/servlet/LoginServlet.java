package fr.acceis.forum.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.acceis.forum.dao.DaoFactory;
import fr.acceis.forum.model.Utilisateur;

public class LoginServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String login = req.getParameter("username");
		String password = req.getParameter("password");
		
		Utilisateur utilisateur = DaoFactory.getUtilisateurDao().getUtilisateurByLoginAndPassword(login, password);
		
		if (utilisateur == null) {
			String message = "Authentification échouée !";
			req.setAttribute("message", message);
			req.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(req, resp);
		} else {
			req.getSession().setAttribute("utilisateur", utilisateur);
			
			resp.sendRedirect("home");
		}
	}
	
}
