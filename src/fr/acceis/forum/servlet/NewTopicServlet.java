package fr.acceis.forum.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.acceis.forum.dao.DaoFactory;
import fr.acceis.forum.dao.IFilDeDiscussionDao;
import fr.acceis.forum.dao.IPostDao;
import fr.acceis.forum.model.FilDeDiscussion;
import fr.acceis.forum.model.Post;
import fr.acceis.forum.model.Utilisateur;

public class NewTopicServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (req.getSession().getAttribute("utilisateur") == null) {
			req.setAttribute("message", "Veuillez vous authentifier !");
			resp.sendRedirect("login");
		}
		else req.getRequestDispatcher("/WEB-INF/jsp/newTopic.jsp").forward(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String titre = req.getParameter("titre");
		String message = req.getParameter("message");

		Utilisateur utilisateur = (Utilisateur) req.getSession().getAttribute("utilisateur");
		
		if (utilisateur == null) {
			req.setAttribute("message", "Veuillez vous identifier pour créer un fil de discussion !");
			resp.sendRedirect("login");
		} else if (titre == null || message == null || "".equals(titre) || "".equals(message)) {
			req.setAttribute("message", "Veuillez remplir tous les champs !");
			req.getRequestDispatcher("/WEB-INF/jsp/newTopic.jsp").forward(req, resp);
		} else {
			IFilDeDiscussionDao filDeDiscussionDao = DaoFactory.getFilDeDiscussionDao();
			IPostDao postDao = DaoFactory.getPostDao();
			
			Date now = GregorianCalendar.getInstance().getTime();
			
			FilDeDiscussion fil = new FilDeDiscussion();
			fil.setAuteur(utilisateur);
			fil.setDate(now);
			fil.setNbVues(0);
			fil.setTitre(titre);
			
			filDeDiscussionDao.save(fil);
			
			Post post = new Post();
			post.setAuteur(utilisateur);
			post.setDate(now);
			post.setTexte(message);
			post.setFil(fil);
			
			postDao.save(post);
			
			resp.sendRedirect("topic?id=" + fil.getId());
		}
	}

}
