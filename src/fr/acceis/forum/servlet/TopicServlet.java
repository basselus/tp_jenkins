package fr.acceis.forum.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.acceis.forum.dao.DaoFactory;
import fr.acceis.forum.dao.IFilDeDiscussionDao;
import fr.acceis.forum.model.FilDeDiscussion;

public class TopicServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		if (id == null || "".equals(id)) {
			req.setAttribute("message", "Veuillez indiquer un id");
			resp.sendRedirect("home");
		} else {
			IFilDeDiscussionDao filDeDiscussionDao = DaoFactory.getFilDeDiscussionDao();
			FilDeDiscussion fil = filDeDiscussionDao.getById(Long.parseLong(id));
			fil.setNbVues(fil.getNbVues() + 1);
			filDeDiscussionDao.save(fil);
			
			req.setAttribute("fil", fil);
			
			req.getRequestDispatcher("/WEB-INF/jsp/thread.jsp").forward(req, resp);
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
