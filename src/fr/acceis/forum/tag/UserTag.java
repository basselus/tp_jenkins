package fr.acceis.forum.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import fr.acceis.forum.model.Utilisateur;

public class UserTag extends SimpleTagSupport {

	private Utilisateur user;

	public void setUser(Utilisateur user) {
		this.user = user;
	}

	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		if (user != null && !"".equals(user)) out.println("Bienvenue " + user.getLogin() + " | ");
		else out.println("<a href=\"login\">Authentifiez-vous</a> | ");
	}

}
